"""
Q-Learning in the Open AI Gym MountainCar environment
Based on work by Moustafa Alzantot (malzantot@ucla.edu)
https://github.com/malzantot/ReinforcementLearning_examples.git
"""

import getopt, gym, numpy as np, sys
np.set_printoptions(precision=3, sign=' ')

from gym import wrappers

monitor = False
render = False
verbose = False
display = False

LEFT = 0
DRIFT = 1
RIGHT = 2
num_moves = 3

num_x_cells = num_v_cells = 40

max_iter_epochs = 10000
max_eval_epochs = 100
max_episodes = 10000
max_exhibits = 100
max_prints = 10

# Learning hyper-params
gamma = 1.0 # reward discount factor
# Utility renewal rate (high = learn fast/low = learn slow)
# eta = max(min_eta, ini_eta * (eta_decay_base ** (epoch//eta_decay_delay)))
ini_eta = 1.0 # Initial learning rate
min_eta = 0.003 # Final learning rate
eta_decay_base = 0.85 # Decay factor base
eta_decay_delay = 100 # Decay attenuation
# Exploration liberty rate
# eps = max(min_eps, ini_eps * (eps_decay_base ** (epoch//eps_decay_delay)))
ini_eps = 1.0 # Initial exploration liberty rate
min_eps = 0.01 # Final exploration liberty rate
eps_decay_base = 0.99 # Decay factor base
eps_decay_delay = 100 # Decay attenuation

#================================================================#
# Enviroment utilities

def depict(move):
  s = ""
  if   move == LEFT:  s = "<"
  elif move == DRIFT: s = "o"
  elif move == RIGHT: s = ">"
  return s

def show_transition(a,b,move,c,d,reward,tally):
  print(a,b,depict(move)," => ",c,d,"[",reward,"/",tally,"]")

def print_utilities(Q_table):
  for m in range(num_moves):
    print("Utilities of move", depict(m))
    #print(Q_table[m,:,:])
    print(" ", end=" ")
    for x in range(num_x_cells):
      print("%6d"%x, end=" ")
    print()
    for y in range(num_v_cells):
      print(y%10, end=" ")
      for x in range(num_x_cells):
        print("%+.3f"%Q_table[m][x][y],end=" ")
      print()

def print_policies(policy):
  num_x_cells, num_v_cells = policy.shape
  #print(policy)
  print(" ", end=" ")
  for x in range(num_x_cells):
    print(x%10, end=" ")
  print()
  for y in range(num_v_cells):
    print(y%10, end=" ")
    for x in range(num_x_cells):
      print(depict(int(policy[x][y])),end=" ")
    print()

#----------------------------------------------------------------#

def run_episode(env, policy=None, render=False):
  new_obs = env.reset()
  tally = 0
  for i in range(max_episodes):
    if render: env.render(mode='ansi')
    cur_obs = new_obs
    a,b = discretise(env, cur_obs) # invoke, for logging
    if policy is None:
      cur_move = env.action_space.sample() # ignore a, b
    else:
      cur_move = policy[a][b]
    new_obs, reward, done, _ = env.step(cur_move)
    c,d = discretise(env, new_obs)
    tally += reward * (gamma ** i)
    if verbose: show_transition(a,b,cur_move,c,d,reward,tally)
    if done: break
  return tally

#----------------------------------------------------------------#

def action_from_oracle(env, cur_obs, cur_move):
  x, v = cur_obs
  if x < env.min_position + 0.1: # too far left
    new_move = RIGHT
  else: # preserve momentum
    if v < -0.001:
      new_move = LEFT
    elif v > 0.001:
      new_move = RIGHT
    else: # exploit gradient
      bottom = -np.pi/6.0 # from _height = np.sin(3 * x)*.45+.55
      if x > bottom + 0.01:
        new_move = LEFT
      elif x < bottom - 0.01:
        new_move = RIGHT
      else: # random move
        new_move = np.random.choice(num_moves)
  return new_move

#----------------------------------------------------------------#

def discretise(env, obs):
  """ Convert a floating point observation into a discrete state """
  env_low = env.observation_space.low
  env_high = env.observation_space.high
  env_dx = (env_high[0] - env_low[0]) / num_x_cells
  env_dv = (env_high[1] - env_low[1]) / num_v_cells
  a = int((obs[0] - env_low[0])/env_dx)
  b = int((obs[1] - env_low[1])/env_dv)
  if verbose: print(obs, "->", a, b)
  return a, b

#----------------------------------------------------------------#

def mostly_utility_maximising_action(env, Q_table, a, b):
  logits = Q_table[:,a,b]
  logits_exp = np.exp(logits)
  probs = logits_exp / np.sum(logits_exp)
  new_move = np.random.choice(num_moves, p=probs)
  if verbose:
    print(logits, logits_exp, probs, depict(new_move))
  return new_move

#----------------------------------------------------------------#

def utility_maximising_actions(env, Q_table):
  policy = np.zeros((num_x_cells, num_v_cells))
  for a in range(num_x_cells):
    for b in range(num_v_cells):
     policy[a][b] = np.argmax(Q_table[:,a,b])
  return policy

#----------------------------------------------------------------#

def reinforcement_learning_algo(env, \
      max_iter_epochs, max_eval_epochs, max_episodes):
  print ("----- Q Learning -----")

  global num_moves
  num_moves = env.action_space.n # LEFT, DRIFT, RIGHT
  Q_table = np.zeros((num_moves, num_x_cells, num_v_cells)) # Q/Utilities

  if verbose: print_utilities(Q_table)
  if display:
    screen, font, clock \
      = launch_display(screen_width, screen_height, "Mountain Car")

  done = halt = quit = False
  for epoch in range(max_iter_epochs):
    new_obs = env.reset()
    cur_move = None
    tally = 0
    # eta = learning rate, decrease at every epoch
    eta = max(min_eta, ini_eta * (eta_decay_base ** (epoch//eta_decay_delay)))
    # eps = exploration liberty, decrease at every epoch
    eps = max(min_eps, ini_eps * (eps_decay_base ** (epoch//eps_decay_delay)))
    for stage in range(max_episodes):
      cur_obs = new_obs
      a, b = discretise(env, cur_obs)
      if np.random.uniform(0, 1) < eps: # exploration: select move at random
        #new_move = np.random.choice(num_moves) # alternative: use training
        new_move = action_from_oracle(env, cur_obs, cur_move)
      else: # select move biassed by current ranking
        new_move = mostly_utility_maximising_action(env, Q_table, a, b)
      cur_move = new_move
      new_obs, reward, done, _ = env.step(cur_move)
      tally += reward
      c, d = discretise(env, new_obs)

      if verbose: show_transition(a,b,cur_move,c,d,reward,tally)
      if display:
        quit, halt = display_mountain_car(env, cur_obs, cur_move, new_obs, \
          epoch, stage, screen, font, clock, quit, halt, reward, tally)

      # Update Q/Utilities
      Q_table[cur_move][a][b] = (1 - eta) * Q_table[cur_move][a][b] \
                              + eta * (reward + gamma * np.max(Q_table[:,c,d]))

      if verbose: print_utilities(Q_table)
      if verbose and 'q' == input("Episode " + str(stage)): sys.exit();
      if done or quit: break
      elif halt:       input("...")

    if epoch > 0 and (epoch+1) % (max_iter_epochs//max_prints) == 0:
      print("Epoch %4d|eta %.3f|eps %.3f|tally %d"%(epoch+1, eta, eps, tally))

    if verbose and 'q' == input("Iteration " + str(i+1)): sys.exit();

  if display:
    time.sleep(3)
    pygame.quit()

  print("Final utilities")
  print_utilities(Q_table)
  opt_policy = np.argmax(Q_table, axis=0)
  assert (opt_policy == utility_maximising_actions(env, Q_table)).all()
  print("Solution:")
  print_policies(opt_policy)
  scores = [
    run_episode(env, opt_policy, False)
    for _ in range(max_eval_epochs)
  ]
  print("Average score of solution = ", np.mean(scores))
  return opt_policy

#================================================================#
# Pygame utilities

import pygame, time

screen_width = 300
screen_height = 200

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)

#----------------------------------------------------------------#

def launch_display(screen_width, screen_height, caption):
  pygame.init()
  screen = pygame.display.set_mode((screen_width, screen_height))
  pygame.display.set_caption(caption)
  font = pygame.font.SysFont("Calibri", 14, True, False)
  clock = pygame.time.Clock()
  return screen, font, clock

def display_mountain_car(env, cur_obs, cur_move, new_obs, \
  epoch, stage, screen, font, clock, quit, halt, reward, tally):

  def elevation(x): # from env._height
    return np.sin(3 * x)*.45+.55 # bottoms at x = pi/6

  def plane_to_screen(env, x):
    x_orig = screen_width/2
    y_orig = 4*screen_height/5
    x_scale = screen_width/4
    y_scale = screen_height/4
    xs = int(x_orig + x_scale * x)
    ys = int(y_orig - y_scale * elevation(x))
    return xs, ys

  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      quit = True
    elif event.type == pygame.KEYDOWN:
      if event.key == pygame.K_q:
        done = True
      elif event.key == pygame.K_RETURN:
        halt = not halt

  screen.fill(BLACK)
  x_new, v_new = new_obs
  y_new = elevation(x_new)
  new_xs, new_ys = plane_to_screen(env, x_new)
  pygame.draw.circle(screen, BLUE, [new_xs,new_ys], 3, 3)
  if x_new >= env.max_position-0.075 or env.min_position >= x_new: # not smaller
    pygame.draw.circle(screen, YELLOW, [new_xs,new_ys], 4, 4)
  elif cur_obs is not None:
    x_cur, _ = cur_obs
    cur_xs, cur_ys = plane_to_screen(env, x_cur)
    pygame.draw.circle(screen, RED, [cur_xs,cur_ys], 3, 3)
    pygame.draw.line(screen, WHITE, [new_xs,new_ys], [cur_xs,cur_ys], 1)
  data = "  " + str("%d"%epoch) + ":" + str("%d"%stage) + "  "
  data += "( " + str("Pos %.3f"%x_new) + " , " \
               + str("Alt %.3f"%y_new) + " , " \
               + str("Vel %.3f"%v_new) + " )"
  if   cur_move is None: data += "     "
  elif cur_move == 0: data += " <<< "
  elif cur_move == 1: data += " ooo "
  elif cur_move == 2: data += " >>> "
  data += " " + str("%.3f"%reward) + " "
  data += " " + str("%.3f"%tally) + " "
  text = font.render(data, True, WHITE)
  screen.blit(text, [0,0])
  pygame.display.flip()
  clock.tick(5)
  return quit, halt

#================================================================#

def main():
  global monitor, render, verbose, display
  global eta_decay_delay, eps_decay_delay, num_x_cells, num_v_cells, \
    max_iter_epochs, max_eval_epochs, max_episodes, max_exhibits, \
    max_prints

  opts, args = getopt.getopt(sys.argv[1:], "n:t:v:s:o:MRVW")
  for opt in opts:
    if   opt[0] == "-n": num_x_cells, num_v_cells = map(int, opt[1].split("x"))
    elif opt[0] == "-t": max_iter_epochs = int(opt[1])
    elif opt[0] == "-v": max_eval_epochs = int(opt[1])
    elif opt[0] == "-s": max_episodes = int(opt[1])
    elif opt[0] == "-o": max_exhibits = int(opt[1])
    elif opt[0] == "-M": monitor = True
    elif opt[0] == "-R": render = True
    elif opt[0] == "-V": verbose = True
    elif opt[0] == "-W": display = True

  eta_decay_delay = eps_decay_delay = max_iter_epochs//100
  if eta_decay_delay < 1: eta_decay_delay = 1
  if eps_decay_delay < 1: eps_decay_delay = 1
  if max_prints > max_iter_epochs or max_prints > max_eval_epochs:
    max_prints = min(max_iter_epochs, max_eval_epochs)

  print(eta_decay_delay, eps_decay_delay, num_x_cells, num_v_cells, \
    max_iter_epochs, max_episodes, max_eval_epochs, max_exhibits)
  #if "q" == input(): sys.exit()

  env_ref = 'MountainCar-v0'
  env = gym.make(env_ref)
  env.seed(0)
  np.random.seed(0)

  # Solution
  opt_policy = reinforcement_learning_algo(env, \
                 max_iter_epochs, max_eval_epochs, max_episodes)
  # Animation
  run_episode(env, opt_policy, render=render)
  if monitor:
    monitor_path = "/tmp/" + sys.argv[0][:-3] + "-qlearning"
    env = wrappers.Monitor(env, monitor_path, force=True)
  #max_episodes *= max_exhibits
  run_episode(env, opt_policy, render=True)
  env.close()

#----------------------------------------------------------------#

if __name__ == '__main__':
  main()

# vim:set ts=2 sw=2 expandtab indentexpr=
