
"""
Optimal policy random search in the Open AI Gym CartPole environment
Based on work by Moustafa Alzantot (malzantot@ucla.edu)
https://github.com/malzantot/ReinforcementLearning_examples.git
"""

import getopt, gym, numpy as np, sys
from gym import wrappers

def generate_random_policy():
  policy = (np.random.uniform(-1,1, size=4), np.random.uniform(-1,1))
  print("policy", policy)
  return policy

def action_from_policy(env, policy, obs):
  r = 0
  if np.dot(policy[0], obs) + policy[1] > 0: r = 1
  return r

def run_episode(env, policy, max_episodes=10000, render=False):
  new_obs = env.reset()
  tally = 0
  for stage in range(max_episodes):
    if render: env.render()
    cur_obs = new_obs
    selected = action_from_policy(env, policy, cur_obs)
    new_obs, reward, done, _ = env.step(selected)
    tally += reward
    print(cur_obs, selected, "-->", new_obs, reward)
    if done: break
  print("tally:", tally)
  return tally

def print_specs(env):
  print("Observation is an array of 4 floats: \n" \
        " position and velocity of the cart \n" \
        " angular position and velocity of the pole \n" \
        "Reward is a scalar float value \n" \
        "Action is a scalar integer with only two possible values: \n" \
        " 0 — move left \n" \
        " 1 — move right" \
        )

def main():
  num_policies = 500
  max_episodes = 10000
  render = False
  describe_env = False

  opts, args = getopt.getopt(sys.argv[1:], "p:s:RD")
  for opt in opts:
    print(opt)
    if   opt[0] == "-p": num_policies = int(opt[1])
    elif opt[0] == "-s": max_episodes = int(opt[1])
    elif opt[0] == "-R": render = True
    elif opt[0] == "-D": describe_env = True

  env = gym.make('CartPole-v0')
  if describe_env: return print_specs(env)

  # Generate a pool of random policies
  policies = [generate_random_policy() for _ in range(num_policies)]

  # Evaluate the score of each policy
  scores = [
    run_episode(env, p, max_episodes=max_episodes, render=render)
    for p in policies
  ]

  # Select the best policy
  print("Best policy score = %.3f"%max(scores))
  optimal_policy = policies[np.argmax(scores)]
  print("Running with an optimal policy:\n")
  env = wrappers.Monitor(env, "/tmp/cartpole-v0", force=True)
  run_episode(env, optimal_policy, max_episodes=max_episodes, render=render)
  env.close()

if __name__ == "__main__":
  main()
