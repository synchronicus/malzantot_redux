# README #

### What is this repository for? ###

* Machine learning code samples by M.Alzantot (malzantot@ucla.edu) reworked.
* Initial Version.

### How do I get set up? ###

* Python3, NumPy, OpenAiGym (gym).
* Optionally PyGame for animations.

### Running the code ###

* Read the main routines for options (near the bottom of the scripts).
* Scripts will run without options, but may take time to complete due to
  default trial lengths and numbers.
* For short (but possibly inconclusive) runs use "-t10 -v10 -s10" with no args.
* Add "-V" for verbose output, "-W" for animations where supported.

### Who do I talk to? ###

* synchronicus@yahoo.co.uk
* synchronicus@hotmail.co.uk

