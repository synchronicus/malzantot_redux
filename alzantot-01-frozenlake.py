
"""
Policy and Value Iteration et al in the Open AI Gym FrozenLake environment
Based on work by Moustafa Alzantot (malzantot@ucla.edu)
https://github.com/malzantot/ReinforcementLearning_examples.git
"""

#================================================================#
import getopt, gym, numpy as np, random, sys, time
np.set_printoptions(precision=3)
from gym import wrappers

X = Y = 4

gamma = 1.0 # discount factor.
eps = 1e-10 # cooling factor

verbose = False
monitor = False
render = False

#================================================================#
# Display auxiliaries
# https://github.com/openai/gym/blob/master/gym/envs/toy_text/frozen_lake.py
LEFT = 0; DOWN = 1; RIGHT = 2; UP = 3; NONE = 4
MAPS = {
    "4x4": [
        "SFFF", # (S: starting point, safe)
        "FHFH", # (F: frozen surface, safe)
        "FFFH", # (H: hole, fall to your doom)
        "HFFG"  # (G: goal, where the frisbee is located)
    ],
    "8x8": [
        "SFFFFFFF",
        "FFFFFFFF",
        "FFFHFFFF",
        "FFFFFHFF",
        "FFFHFFFF",
        "FHHFFFHF",
        "FHFFHFHF",
        "FFFHFFFG"
    ],
}

ACTION_CODES = [LEFT, DOWN, RIGHT, UP]
ACTION_SYMBOLS = ["<<", "\\/", ">>", "/\\"]
def depict(a): return ACTION_SYMBOLS[a]
def encode(m): return ACTION_CODES[m]

def coords_of_state(s): return s//X, s%X
def state_of_coords(x,y): return y*X + x

def print_state_of_play(s):
  text = ""
  sx, sy = coords_of_state(s)
  mapkey = str(X)+"x"+str(Y)
  for y in range(Y):
    for x in range(X):
      if x == sx and y == sy: text += "R"
      else: text += MAPS[mapkey][y][x]
    if y < Y-1: text += "\n"
  print(text)

def print_valuations(V):
  text = ""
  for y in range(Y):
    for x in range(X):
      text += str("  %1.3f"%V[y*X+x])
    if y < Y-1: text += "\n"
  print(text)

def print_policies(policy):
  text = ""
  for y in range(Y):
    for x in range(X):
      text += str("  %s"%depict(int(policy[y*X+x])))
    if y < Y-1: text += "\n"
  print(text)

#================================================================#
# Peek at internals
def tour_of_inspection(env):
  global verbose
  verbose_general = verbose
  print("Action space:", env.action_space, "[", env.nA , "]")
  assert env.nA == env.action_space.n
  print("States:", X, "*", Y, "=", env.nS, "grid cells")

  # Basic demo:
  policy = random_policy()
  score = evaluate_policy(env, policy, \
                          max_eval_epochs=1, max_episodes=10, render=True)
  print("Random policy score:", score);
  print("------------------------------")
  if "q" == input(">... "): sys.exit()

  if X == 4 and Y == 4:
    policy = [
      RIGHT, RIGHT, DOWN,  LEFT,
      DOWN,  NONE,  DOWN,  NONE,
      RIGHT, RIGHT, DOWN,  NONE,
      NONE,  RIGHT, RIGHT, NONE
    ] # an "obvious" policy: Take the shortest route to the goal ...
    verbose = True
    run_episode(env, policy, max_episodes=env.nS, render=True)
    verbose = verbose_general
    score = evaluate_policy(env, policy, max_eval_epochs=1000, max_episodes=100)
    print('Obvious policy average score = ', score) # ... scores low
    print("------------------------------")
    if "q" == input(">... "): sys.exit()
    policy = [
      LEFT, UP,    UP,   UP,
      LEFT, LEFT,  LEFT, LEFT,
      UP,   DOWN,  LEFT, LEFT,
      LEFT, RIGHT, DOWN, LEFT
    ] # a "hindsight" policy: Turn away from the holes ...
    verbose = True
    run_episode(env, policy, max_episodes=env.nS, render=True)
    verbose = verbose_general
    score = evaluate_policy(env, policy, max_eval_epochs=1000, max_episodes=100)
    print('Hindsight policy average score = ', score) # ... scores high
    print("------------------------------")
    if "q" == input(">... "): sys.exit()

  # Here's why: The agent mostly deviates from the policy, but only sideways
  print("POS ACT [PROB of]  POS R  [PROB of]  POS R  ...")
  for a in range(env.nA): # total number of actions
    print("Action ", depict(a))
    for s in range(env.nS): # total number of states
      sx, sy = coords_of_state(s)
      m = depict(a)
      text = "%d,%d %s  "%(sx,sy,m)
      for prob, s_new, rew, done in env.P[s][a]:
        sx_new, sy_new = coords_of_state(s_new)
        text += "[%1.3f] => %d,%d %d  "%(prob,sx_new,sy_new,rew)
      print(text)
    if "q" == input(">... "): sys.exit()

  assert X*Y == env.nS
  env.close()

#================================================================#
# Basic runner
def run_episode(env, policy, max_episodes=0, render=False):
  """ Evaluate policy by testing it during an episode collecting the rewards

  args:
  env: gym environment.
  policy: the policy to be used.
  max_episodes: maximum number of episodes (0 for unlimited)
  render: boolean to turn rendering on/off.

  returns:
  tally: accumulated reward received by agent under policy
  """
  new_obs = env.reset()
  reward = 0
  tally = 0
  stage = 0
  if verbose: print_state_of_play(new_obs)
  while max_episodes == 0 or stage < max_episodes:
    if render: env.render(mode="ansi")
    cur_obs = new_obs
    cur_act = int(policy[cur_obs])
    new_obs, reward, done, _ = env.step(cur_act)
    tally += (gamma ** stage * reward)
    if verbose:
      cx, cy = coords_of_state(cur_obs)
      nx, ny = coords_of_state(new_obs)
      m = depict(cur_act)
      if verbose:
        print("%d,%d %s  ==>  %d,%d %d[%d]"%(cx,cy,m,nx,ny,reward,tally), done)
        print_state_of_play(new_obs)
    stage += 1
    if done: break
  return tally

#================================================================#
def evaluate_policy(env, policy, max_eval_epochs, max_episodes, render=False):
  """ Evaluate a policy in a number of episodes
  returns:
  average cumulative yield
  """
  scores = [
    run_episode(env, policy, max_episodes, render=False)
    for _ in range(max_eval_epochs)
  ]
  return np.mean(scores)

#----------------------------------------------------------------#
def extract_policy(env, V):
  """ Extract policy given a valuation """
  policy = np.zeros(env.nS)
  for s in range(env.nS):
    if verbose:
      Q_s = np.zeros(env.nA)
      x, y = coords_of_state(s)
      for a in range(env.nA):
        m = depict(a)
        for outcome in env.P[s][a]:
          # outcome is a quadruple (probability, next-state, reward, done)
          prob, s_new, rew, _ = outcome
          Q_s[a] += (prob * (rew + gamma * V[s_new]))
          x_new, y_new = coords_of_state(s_new)
          print("Q(%d,%d,%s)"%(x,y,m), \
                "+= %.3f * (%d + %.3f * V[%d,%d]=%.3f)" \
                %(prob,rew,gamma,x_new,y_new,V[s_new]))
      print("Q(%d,%d)="%(x,y), Q_s, "-> pol[%d,%d]=%d"%(x,y,policy[s]))
    else:
      Q_s = [
        sum([p * (r + gamma * V[s_]) for p, s_, r, _ in  env.P[s][a]])
        for a in range(env.nA)
      ]
    policy[s] = np.argmax(Q_s) # an action a of maximal utlity Q_s[a] in state s
  print("Policy final:")
  print_policies(policy)
  return policy

#================================================================#
# Policy iteration part
def compute_valuation_from_policy(env, policy):
  """ Iteratively evaluate the value-function under policy.
  Alternatively, we could formulate a set of linear equations in iterms of V[s]
  and solve them to find the value function.
  """
  V_new = np.zeros(env.nS)
  while True:
    V_cur = np.copy(V_new)
    for s in range(env.nS):
      pol = policy[s]
      V_new[s] = sum([p * (r + gamma * V_cur[s_]) for p, s_, r, _ in env.P[s][pol]])
    if np.sum((np.fabs(V_cur - V_new))) < eps: break # value converged
  return V_new

#----------------------------------------------------------------#
def policy_iteration_algo(env, max_iter_epochs):
  """ Policy-Iteration algorithm """
  new_policy = np.random.choice(env.nA, size=(env.nS))  # start on random policy
  for i in range(max_iter_epochs):
    cur_policy = new_policy
    V_cur = compute_valuation_from_policy(env, cur_policy)
    new_policy = extract_policy(env, V_cur)
    if np.all(cur_policy == new_policy):
      print ("Policy-Iteration converged at step", i+1)
      break
  return new_policy

#----------------------------------------------------------------#
def policy_iteration(env, max_iter_epochs, max_eval_epochs, max_episodes):
  opt_policy = policy_iteration_algo(env, max_iter_epochs)
  score = evaluate_policy(env, opt_policy, max_eval_epochs, max_episodes)
  print("Policy score = %.3f"%score)
  return opt_policy

#================================================================#
# Value iteration part
def value_iteration_algo(env, max_iter_epochs):
  """ Value Iteration algorithm """
  V_new = np.zeros(env.nS)  # initialize value-function
  if verbose: print("V", V_new)
  for i in range(max_iter_epochs):
    V_cur = np.copy(V_new)
    for s in range(env.nS):
      if verbose:
        Q_s = np.zeros(env.nA)
        x, y = coords_of_state(s)
        text = "%d,%d:\n"%(x,y)
        for a in range(env.nA):
          m = depict(a)
          for prob, s_new, rew, _ in env.P[s][a]:
            Q_s[a] += prob * (rew + V_cur[s_new])
            x_new, y_new = coords_of_state(s_new)
            text += "[%s {%.3f %d} -> (%d,%d)]"%(m,prob,rew,x_new,y_new)
          if a < env.nA - 1: text += "\n"
        print(text)
        print("Q(%d,%d) ="%(x,y), Q_s, "--> V(%d,%d)=%.3f"%(x,y,V_new[s]))
      else:
        Q_s = [
          sum([p * (r + V_cur[s_]) for p, s_, r, _ in env.P[s][a]])
          for a in range(env.nA)
        ]
      V_new[s] = max(Q_s)
    if verbose:
      print("V new:", V_new)
      print_valuations(V_new)
      if "q" == input(">... "): sys.exit()
    if (np.sum(np.fabs(V_cur - V_new)) < eps):
      print ("Value Iteration converged after step", i+1)
      break
  print("V final:", V_new)
  print_valuations(V_new)
  return V_new

#----------------------------------------------------------------#
def value_iteration(env, max_iter_epochs, max_eval_epochs, max_episodes):
  V_optimal = value_iteration_algo(env, max_iter_epochs);
  policy = extract_policy(env, V_optimal)
  score = evaluate_policy(env, policy, max_eval_epochs, max_episodes)
  print("Policy score = %.3f"%score)
  return policy

#================================================================#
# Genetic algorithm part
def random_policy():
  return [np.random.choice(4) for _ in range(16)]

def mutate(policy, prob):
  new_policy = [x for x in policy]
  for i in range(len(policy)):
    if random.random() < prob: new_policy[i] = np.random.choice(4)
  return new_policy

def crossover(policy1, policy2):
  new_policy = [x for x in policy1]
  for i in range(len(policy1)):
    if random.random() < 0.5: new_policy[i] = policy2[i]
  return new_policy

def genetic_algorithm(env, \
      num_policies, num_crossovers, num_mutates, num_generations, \
      max_iter_epochs, max_eval_epochs, max_episodes):

  policy_pool = [random_policy() for _ in range(num_policies)]

  start = time.time()
  print(" Gen :    Min :    Max")
  for i in range(num_generations):
    scores = [
      evaluate_policy(env, p, max_eval_epochs, max_episodes)
      for p in policy_pool
    ]
    print("%4d : %4.4f : %4.4f"%(i+1, np.min(scores), np.max(scores)))
    rank = np.argsort(scores)
    new_pool = [policy_pool[x] for x in rank[-20:]] # top 20
    # cross-over
    crossovers = [
      crossover(random.choice(new_pool), random.choice(new_pool))
      for _ in range(num_crossovers)]
    # mutation
    mutates = [
      mutate(random.choice(new_pool), 0.03)
      for _ in range(num_mutates)
    ]
    new_pool += crossovers
    new_pool += mutates
    policy_pool = new_pool

  end = time.time()
  time_taken = end - start
  print("Time taken = %4.4f" %time_taken)

  if verbose:
    scores = []
    top = -1
    i = 0
    for pol in policy_pool:
      score = evaluate_policy(env, pol, max_eval_epochs, max_episodes)
      scores.append(score)
      if score > top:
        top = score
        print("Pol", i, "score", top)
        print_policies(pol)
      i += 1
  else:
    scores = [
      evaluate_policy(env, pol, max_eval_epochs, max_episodes)
      for pol in policy_pool
    ]
  print("Average policy score = %.3f"%np.mean(scores))
  opt_policy = policy_pool[np.argmax(scores)]
  print("Top scorer with", np.max(scores), "at pool position", np.argmax(scores))
  print_policies(opt_policy)
  return opt_policy

#================================================================#
def main():
  quickcheck = False
  monitor = False
  render = False
  global verbose

  algos = ["pol-iter", "val-iter", "genetic"]
  algo = 0
  global X, Y, gamma, eps

  # Tuning hyper-params (defaults for the 4x4 version)
  max_iter_epochs = 100000
  max_eval_epochs = 100000
  max_episodes = 100
  max_exhibits = 100

  # GA
  num_policies = 50; num_crossovers = 20; num_mutates = 10; num_generations = 50

  opts, args = getopt.getopt(sys.argv[1:], "a:n:g:e:t:v:s:o:QMRV", [
      "num-policies=", "num-crossovers=", "num-mutates=", "num-generations="
    ]
  )
  for opt, arg in opts:
    # Algorithm and tuning params
    if   opt == "-a": algo = int(arg)
    elif opt == "-n": X = Y = int(arg)
    elif opt == "-g": gamma = float(arg)
    elif opt == "-e": eps = float(arg)
    elif opt == "-t": max_iter_epochs = int(arg)
    elif opt == "-v": max_eval_epochs = int(arg)
    elif opt == "-s": max_episodes = int(arg)
    elif opt == "-o": max_exhibits = int(arg)
    # Meta params
    elif opt == "-Q": quickcheck = True
    elif opt == "-M": monitor = True
    elif opt == "-R": render = True
    elif opt == "-V": verbose = True
    # GA only
    elif opt == "--num-policies": num_policies = int(arg)
    elif opt == "--num-crossovers": num_crossovers = int(arg)
    elif opt == "--num-mutates": num_mutates = int(arg)
    elif opt == "--num-generations": num_generations = int(arg)

  env_ref = "FrozenLake-v0"
  if X == 8 and Y == 8:
    env_ref = "FrozenLake"+str(X)+"x"+str(Y)+"-v0"
  env = gym.make(env_ref)
  if quickcheck:
    return tour_of_inspection(env)
  if algos[algo] == "pol-iter":
    opt_policy = policy_iteration(env, \
      max_iter_epochs, max_eval_epochs, max_episodes)
  elif algos[algo] == "val-iter":
    opt_policy = value_iteration(env, \
      max_iter_epochs, max_eval_epochs, max_episodes)
  elif algos[algo] == "genetic":
    opt_policy = genetic_algorithm(env, \
      num_policies, num_crossovers, num_mutates, num_generations, \
      max_iter_epochs, max_eval_epochs, max_episodes)
  if monitor:
    monitor_path = "/tmp/" + sys.argv[0][:-3] + "-" + algo
    env = wrappers.Monitor(env, monitor_path, force=True)
  for _ in range(max_exhibits):
    run_episode(env, opt_policy, max_episodes=0, render=render)
  env.close()

#================================================================#
if __name__ == '__main__':
  main()

#vim:set ts=2 sw=2 expandtab indentexpr=
